package com.atlassian.stash.importer

import java.io.{OutputStream, FileFilter, IOException, File}
import scala.sys.process._

object NativeGitAgent {
  val MIN_VERSION = "1.7.3"
}

class NativeGitAgent(gitPath: Option[String], username: String, password: String, debugLogger: Logger) extends GitAgent {
  import debugLogger._

  object VersionComparator extends Ordering[String] {
    def atoi(s: String) = {
      var i = 0
      while (i < s.length && Character.isDigit(s(i))) i = i + 1
      try {
        s.substring(0, i).toInt
      } catch {
        case e: NumberFormatException => 0
      }
    }

    override def compare(l: String, r: String): Int = {
      val lefts = l.split("[-._]")
      val rights = r.split("[-._]")
      var i = 0
      var v = 0
      while (i < math.min(lefts.length, rights.length) && v == 0) {
        v = atoi(lefts(i)).compareTo(atoi(rights(i)))
        i = i + 1
      }
      if (v != 0) v else lefts.length.compareTo(rights.length)
    }
  }

  val gitVersion =
    (try {
      lines(Seq("--version"), None).headOption
    } catch { case e: IOException => None })
      .flatMap("(?<=version )[^ ]+".r findFirstIn _)

  if (gitVersion.isEmpty) throw new IllegalArgumentException("Unable to determine git version")
  if (VersionComparator.compare(NativeGitAgent.MIN_VERSION, gitVersion.get) > 0) throw new IllegalArgumentException("Git %s or greater is required".format(NativeGitAgent.MIN_VERSION))

  def setRemote(remoteName: String, remoteUrl: String, repo: File): Either[String, String] = {
    Either.cond(apply(Seq("remote", "rm", remoteName), Some(repo)) ### apply(Seq("remote", "add", remoteName, remoteUrl), Some(repo)) ! newProcessLogger() == 0,
      remoteName, "Error creating Git remote: " + remoteUrl)
  }

  def push(remote: String, repoDir: File): Either[String, String] = {
    val gitPushAll = apply(Seq("push", "-v", "--progress", "--all", remote), Some(repoDir))
    val gitPushTags = apply(Seq("push", "-v", "--progress", "--tags", remote), Some(repoDir))

    //'git push --all' && 'git push --tags'
    Either.cond(
      (gitPushAll #&& gitPushTags) ! newProcessLogger(debugLogger, debugLogger) == 0,
      "Successfully pushed to Stash",
      "Pushing to Stash failed")
  }

  override protected def isGitDir(dir: File) =
    hasDotGitDir(dir) &&
    apply(Seq("rev-parse"), Some(dir.getAbsoluteFile)) ! newProcessLogger() == 0 //git rev-parse returns 0 for git dire, non-zero for non-git dirs

  private def newProcessLogger(out: Logger = debugLogger, err: Logger = debugLogger): ProcessLogger = {
    ProcessLogger(s => out.log("out> " + s), s => err.log("err> " + s))
  }

  private def lines(cmd: Seq[String], cwd: Option[File] = None): Stream[String] =
    apply(cmd, cwd).lines(newProcessLogger()).map(s => { logReturn("out> " + s); s })

  private def apply(cmd: Seq[String], cwd: Option[File], env: (String, String)*) =
    logWith("run(cwd=" + cwd.map(_.getAbsolutePath).getOrElse("NONE") + ")> " + (gitPath.getOrElse("git") +: cmd).mkString(" "))(
      Process(gitPath.getOrElse("git") +: cmd, cwd))
}
