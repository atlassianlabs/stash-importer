package com.atlassian.stash.importer

import org.clapper.argot._
import ArgotConverters._
import java.io.File
import io.{BufferedSource, Source}
import ImportState._

object ImportList extends ImportCommand {
  def name = "importlist" //must be def not val because needed before this class is initialised
  val description = "Imports multiple git repositories from the local filesystem into Stash. Uses an import list file created by the createlist command."

  private val inListFile = parser.parameter[String]("inlist", "Path to the import list file.", optional = false)
  private val outListFile = parser.parameter[String]("outlist", "Where to write the updated import list. If omitted the original import list file is overwritten.", optional = true)

  override def run(git: GitAgent, stash: Stash, console: Logger) : Boolean = {
    import console.{ log => conLog }

    val file = new File(inListFile.value.get)

    conLog("Importing repositories from import list \"%s\"".format(file.getAbsolutePath))

    if (!file.exists() || !file.canRead()) {
      conLog("Unable to read import list file \"%s\"".format(file.getAbsolutePath))

      false
    } else {
      val lineResults: Seq[Either[ImportLineError, ImportOutcome]] = processLines(Source.fromFile(inListFile.value.get, "UTF-8"))
      val errors = lineResults.filter(_.isLeft).map(_.left.get).toSeq
      val imports = lineResults.filter(_.isRight).map(_.right.get).toSeq
      if (!errors.isEmpty) {
        errors.foreach(lineNumAndError => {
          conLog("An error occurred while processing line %d: %s".format(lineNumAndError.line, lineNumAndError.message), lineNumAndError.exception);
        })

        false
      } else {
        if (imports.isEmpty) {
          conLog("No repositories imported. None specified for import.")

          true
        } else if (imports.forall(_.state == Succeeded)) {
          conLog("No repositories imported. All were already imported.")

          true
        } else {
          val results = importRepos(imports, git, stash, console)

          printResultSummary(imports, results, console)
          writeResults(results, console)

          results.forall(_.state == Succeeded)
        }
      }
    }
  }

  private def processLines(linesSource: BufferedSource): Seq[Either[ImportLineError, ImportOutcome]] = {
    def isValidImportState(name: String): Boolean = {
      try {
        ImportState.withName(name)
        true
      }
      catch {
        case e: NoSuchElementException => false
      }
    }

    def processLine(line: String, lineNum: Int): Option[Either[ImportLineError, ImportOutcome]] = {
      try {
        if (line.startsWith("#") || line.trim.length == 0) None
        else {
          val lineValues: Array[String] = line.split(",").padTo(6, "").map(_.trim)
          if (lineValues.take(5).exists(_.length == 0)) {
            //If any but the comment fields are empty, the line is invalid
            Some(Left(ImportLineError(lineNum, "Some mandatory values are empty: " + line)))
          } else if (!isValidImportState(lineValues(4))) {
            //If the import state value is invalid
            Some(Left(ImportLineError(lineNum, "The import state value is invalid. Valid values: " + ImportState.values.mkString(", "))))
          } else {
            val Array(dir: String, projKey: String, projName: String, repoName: String, stateString: String, comment: String) = lineValues
            val state = ImportState.withName(stateString)
            val step = ImportStep(new File(dir), projKey, projName, repoName)
            Some(Right(
              state match {
                case Succeeded => ImportSuccess(step, comment)
                case Pending => ImportPending(step, comment)
                case Failed => ImportError(step, comment)
              }))
          }
        }
      }
      catch {
        case e: Exception => Some(Left(ImportLineError(lineNum, e.getMessage, Some(e))))
      }
    }

    (for (lineAndNum <- linesSource.getLines().zipWithIndex;
         processed <- processLine(lineAndNum._1, lineAndNum._2)) yield processed).toSeq
  }

  private def writeResults(results: Seq[ImportOutcome], console: Logger) {
    import console.{ log => conLog }

    val outFile = resolvePath(outListFile.value.getOrElse(inListFile.value.get))
    val actualFile =
      try {
        writeImportOutcomes(outFile, results)
        outFile
      }
      catch {
        case e: Exception => {
          val tempFile = File.createTempFile("import-results", ".list")
          conLog("Failed to write updated import list to \"%s\": %s".format(outFile, e.getMessage))
          writeImportOutcomes(tempFile, results)
          tempFile
        }
      }
    conLog("Updated import list written to \"%s\"".format(actualFile))
  }

  private def printResultSummary(imports: Seq[ImportOutcome], results: Seq[ImportOutcome], console: Logger) {
    import console.{ log => conLog }

    def countOutcomeTypes(outcomes: Seq[ImportOutcome]): (Int, Int, Int) =
      outcomes.foldLeft((0, 0, 0)) {
        (tally, outcome) => outcome.state match {
          case Succeeded => (tally._1 + 1, tally._2, tally._3)
          case Failed => (tally._1, tally._2 + 1, tally._3)
          case Pending => (tally._1, tally._2, tally._3)
        }
      }
    val before = countOutcomeTypes(imports)
    val after = countOutcomeTypes(results)
    conLog("\nImported %d %s, %d %s failed to import, %d %s were skipped.".format(
        (after._1 - before._1), Pluralize.pluralize(after._1 - before._1, "repository", "repositories"),
        (after._2), Pluralize.pluralize(after._2, "repository", "repositories"),
        (after._3 - before._3), Pluralize.pluralize(after._3 - before._3, "repository", "repositories")))
  }

  private def importRepos(imports: Seq[ImportOutcome], git: GitAgent, stash: Stash, console: Logger): Seq[ImportOutcome] = {
    imports.toSeq.map(step => importRepo(step, git, stash, console))
  }
}

