package com.atlassian.stash.importer

import org.clapper.argot.ArgotConverters._
import java.io.{FileWriter, PrintWriter, File}
import ImportState._
import RepoState._
import annotation.tailrec

abstract class ImportCommand extends Command {
  protected val consoleDebugFlag = parser.flag[Boolean](List("d", "debug"), "Log more information to the console.")
  protected val nativeGitFlag = parser.flag[Boolean](List("n", "nativegit"), "Whether to use native git or JGit.")
  protected val gitPathOpt = parser.option[String](List("g", "git"), "path", "Path to the git executable.")
  protected val usernameOpt = parser.option[String](List("u", "username"), "username", "Username for a Stash account with system administration privileges.")
  protected val passwordOpt = parser.option[String](List("p", "password"), "password", "Password for the Stash account with system administration privileges. Optional. If omitted you will be prompted for the password on the command line.")
  protected val stashUrlParam = parser.parameter[String]("stash", "URL to the Stash instance that will import the repositories.", optional = false)

  final def apply(args: Array[String], fileLogger: Logger) = {
    parser.parse(args)

    val sysOutLogger: Logger = new PrintLogger(new PrintWriter(System.out))
    val debugLogger: Logger = if (consoleDebugFlag.value.isDefined) new TeeLogger(fileLogger, sysOutLogger) else fileLogger
    val consoleLogger: Logger = new TeeLogger(sysOutLogger, fileLogger)

    debugLogger.log(
      "\n===================================================================================================" +
      "\nTime: %s" +
      "\nCommand: %s\n" format(new java.util.Date(), sanitiseArgs(args).mkString(" ")))

    val username = usernameOpt.value.getOrElse({ print("Enter username for [" + stashUrlParam.value.get + "]: "); readLine() })
    val password = passwordOpt.value.getOrElse({
      print("Enter password for [" + stashUrlParam.value.get + "]: ")
      try {
        new String(System.console().readPassword())
      }
      catch {
        case e: NoSuchMethodError => readLine()
      }
    })

    val git: GitAgent = if (nativeGitFlag.value.isDefined) new NativeGitAgent(gitPathOpt.value, username, password, debugLogger) else new JGitAgent(username, password, debugLogger)
    val url = if (stashUrlParam.value.get.endsWith("/")) stashUrlParam.value.get.dropRight(1) else stashUrlParam.value.get
    val stash: Stash = new Stash(url, username, password, debugLogger)

    run(git, stash, consoleLogger)
  }

  protected def run(git: GitAgent, stash: Stash, console: Logger): Boolean

  protected def sanitiseArgs(args: Array[String]): Array[String] = {
    //Don't show password params
    val span = args.span(arg => arg != "-p" && arg != "--password")
    if (span._2.length > 1) {
      span._2(1) = "*****"
    }
    span._1 ++ span._2
  }

  protected def importRepos(dirs: Seq[File], projKey: Option[String], projName: Option[String],
                            git: GitAgent, stash: Stash, console: Logger,
                            lookInSubDirs: Boolean = true): Either[Seq[PreImportError], Seq[ImportOutcome]] =
    git.findGitRepos(dirs).fold(
      errors => Left(errors),
      dirs => Right(dirs.map(dir => {
        val legalProjectKey = projKey.orElse(stash.toLegalProjectKey(dir.getName)).getOrElse(stash.chooseProjectKey())
        importRepo(
          ImportPending(
            ImportStep(
              dir,
              legalProjectKey,
              projName.getOrElse(stash.toLegalProjectName(dir.getName).getOrElse(stash.chooseProjectName())),
              stash.toLegalRepoName(dir.getName).getOrElse(stash.chooseRepoName(legalProjectKey)))),
          git, stash, console)
      })))

  protected def importRepo(previous: ImportOutcome, git: GitAgent, stash: Stash, console: Logger): ImportOutcome = {
    import console.{ log => conLog, logReturn => conLogReturn }

    previous.state match {
      case Succeeded => previous //Already imported
      case _ => {
        //Not yet imported or failed to import last time
        conLog()
        conLog("Importing repository in directory \"%s\" into repository %s of project %s" format(previous.step.dir.getAbsolutePath, previous.step.repoName, previous.step.projKey))

        val step = previous.step

        stash.ensureProjectCreated(step.projKey, step.projName).fold(
          error => ImportError(step, conLogReturn("Failed to create project for repository \"%s\": %s".format(step.dir, error))),
          isNewProj => {
            if (isNewProj) {
              conLog("Created project \"%s\"".format(step.projKey))
            }

            ensureCreatedAndPushRepo(stash, step, git, console)
          })
      }
    }
  }

  private def ensureCreatedAndPushRepo(stash: Stash, step: ImportStep, git: GitAgent, console: Logger): ImportOutcome = {
    import console.{ log => conLog, logReturn => conLogReturn }

    stash.ensureRepoCreated(step.projKey, step.repoName, failIfAlreadyExists = false).fold(
      error => ImportError(step, conLogReturn("Failed to create Stash repository for repository \"%s\": %s" format(step.dir, error))),
      success => {
        val (isNewRepo, cloneUrl) = success

        if (isNewRepo) {
          conLog("Created repository \"%s\" in project \"%s\".".format(step.repoName, step.projKey))
        }

        handleRepoPush(isNewRepo, waitForRepoToInit(step, stash, console), step, cloneUrl, git, stash, console)
      })
  }

  private def handleRepoPush(isNewRepo: Boolean, repoState: RepoState.RepoState, step: ImportStep, cloneUrl: String, git: GitAgent, stash: Stash, console: Logger): ImportOutcome = {
    import console.{ logReturn => conLogReturn }

    if (isNewRepo && repoState != Available) {
      ImportError(step, conLogReturn("Could not push \"%s\" to repository \"%s\" in project \"%s\" because Stash failed to initialise it in time (repository state = %s).".format(step.dir, step.repoName, step.projKey, repoState)))
    } else {
      if (isNewRepo) {
        //It's new, so no need to check anything else... just push
        push(cloneUrl, step, git, console)
      } else {
        stash.repoHasCommits(step.projKey, step.repoName).fold(
          //Failed to determine if the repo has commits
          error => ImportError(step, error),
          //Have determined if the repo has commits
          alreadyHasCommits =>
            if (alreadyHasCommits) {
              ImportError(step, conLogReturn("Could not push \"%s\" to repository \"%s\" in project \"%s\" because it is not empty.".format(step.dir, step.repoName, step.projKey)))
            } else {
              //All good - repo does not already have commits. Let's try pushing now.
              push(cloneUrl, step, git, console)
            })
      }
    }
  }

  private def waitForRepoToInit(step: ImportStep, stash: Stash, console: Logger): RepoState = {
    import console.{ log => conLog }

    @tailrec
    def getRepoState(tryTimes: Int, delaySec: Int, lastResult: RepoState): RepoState = {
      if (tryTimes <= 0 || lastResult == Available || lastResult == InitialisationFailed) lastResult
      else {
        conLog("Waiting %ds for repository to initialise...".format(delaySec))
        Thread.sleep(delaySec * 1000)
        getRepoState(tryTimes - 1, delaySec * 2, stash.getRepoState(step.projKey, step.repoName))
      }
    }

    getRepoState(3, 2, stash.getRepoState(step.projKey, step.repoName))
  }


  private def push(cloneUrl: String, step: ImportStep, git: GitAgent, console: Logger): ImportOutcome = {
    import console.{ logWith => conLogWith }

    git.setRemote("stash", cloneUrl, step.dir).fold(
      error => ImportError(step, error),
      result => conLogWith("Pushing \"%s\" to repository \"%s\".".format(step.dir, step.repoName))(
        git.push("stash", step.dir).fold(
          error => conLogWith("Pushing failed.")(ImportError(step, error)),
          result => conLogWith("Pushing succeeded.")(ImportSuccess(step, "\"%s\" was pushed to repository \"%s\" in project \"%s\".".format(step.dir, step.repoName, step.projKey))))))
  }

  protected def writeImportOutcomes(file: File, outcomes: Seq[ImportOutcome]) {
    file.getParentFile.mkdirs()
    file.createNewFile()
    val writer = new PrintWriter(new FileWriter(file))
    writeImportOutcomes(writer, outcomes)
    writer.close()
  }

  protected def resolvePath(path: String) : File = {
    val file = new File(path)
    if (!file.isAbsolute) {
      new File(new File(System.getProperty("user.dir")), path).getCanonicalFile
    } else {
      file
    }
  }

  private def writeImportOutcomes(writer: PrintWriter, outcomes: Seq[ImportOutcome]) {
    writer.println("## Each line represents a local repository that has already been or will")
    writer.println("## be imported along with their project key, project name and repository name")
    writer.println("##")
    writer.println("## If a repository is not yet imported its state will be Ready")
    writer.println("## If a repository has already been successfully imported its state will be Succeeded")
    writer.println("## If a repository failed to import last time its state will be Failed")
    writer.println("## A comment may also be present explaining the reason for the current state")
    writer.println("##")
    writer.println("## Failed repository imports will be retried the next time the importlist command is run.")
    writer.println("## Repositories not yet imported will be attempted the next time the importlist command is run")
    writer.println("## Already imported repositories will be ignored")
    writer.println("##")
    writer.println("## Columns:")
    writer.println("## directory, project key, project name, repository name, import state, comments")
    writer.println()

    def sanitiseComment(comment: String) = comment.replaceAll("(?:\\r?\\n|,)+", " ")

    outcomes.foreach(result => writer.println(List(result.step.dir.getCanonicalFile, result.step.projKey, result.step.projName, result.step.repoName, result.state, sanitiseComment(result.comment)).mkString(", ")))
    writer.flush()
  }
}