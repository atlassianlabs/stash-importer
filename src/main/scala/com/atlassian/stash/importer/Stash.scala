package com.atlassian.stash.importer

import com.sun.jersey.api.client.{UniformInterfaceException, Client}
import util.parsing.json.{JSONObject, JSON}
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter
import java.util.Locale
import java.net.URL
import javax.ws.rs.core.MediaType
import java.text.Normalizer
import RepoState._

object Stash {
  val DEFAULT_PAGE_SIZE = 50
}

class Stash(stashURL : String, username: String, password: String, logger: Logger) {
  private val resource = createResource
  private var projKeysAndNames = getProjectKeysAndNames
  private var projKeys = projKeysAndNames.keys.toSet
  private var projNames = projKeysAndNames.values.toSet
  private var repoNamesByProjKey: Map[String, Set[String]] = Map()

  private def createResource = {
    val client: Client = Client.create()
    client.addFilter(new HTTPBasicAuthFilter(username, password))
    client.resource(stashURL + "/rest/api/latest")
  }

  private def fetchPage[T](path: String, f: Map[String, Any] => T, start: Int = 0, limit: Int = Stash.DEFAULT_PAGE_SIZE): (Option[Int], T) = {
    val response: String = resource.path(path)
      .queryParam("start", start.toString).queryParam("limit", limit.toString)
      .`type`(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
      .get(classOf[String])
    val page = JSON.parseFull(response).get.asInstanceOf[Map[String, Any]]
    val nextPageStart = page.get("nextPageStart").map(start => start.asInstanceOf[Double].toInt)

    (nextPageStart, f(page))
  }

  private def forEachPage[S, T](initial: S, f: (Int, S) => (Option[Int], S)) = {
    var result: S = initial

    var pageStart: Option[Int] = Some(0)

    while (pageStart.isDefined) {
      val (nextPageStart, next) = f(pageStart.get, result)
      result = next
      pageStart = nextPageStart
    }

    result
  }

  private def getProjectKeysAndNames: Map[String, String] = {
    def fetchPageOfResults(start: Int): (Option[Int], Seq[(String, String)]) =
      fetchPage("/projects",
        response => for (project: Map[String, Any] <- response("values").asInstanceOf[Seq[Map[String, Any]]]) yield (project("key").asInstanceOf[String], project("name").asInstanceOf[String]),
        start)

    forEachPage(Map[String, String](), (pageStart, keysAndNames: Map[String, String]) => {
      val (nextPageStart, pageOfKeysAndNames) = fetchPageOfResults(pageStart)
      (nextPageStart, (keysAndNames ++ pageOfKeysAndNames))
    })
  }

  private def getRepoNames(projKey: String): Set[String] = {
    def fetchPageOfResults(projKey: String, start: Int): (Option[Int], Seq[String]) =
      fetchPage("/projects/%s/repos".format(projKey),
        response => for (project: Map[String, Any] <- response("values").asInstanceOf[Seq[Map[String, Any]]]) yield project("name").asInstanceOf[String],
        start)

    forEachPage(Set[String](),
      (pageStart, names: Set[String]) => {
        val (nextPageStart, pageOfNames) = fetchPageOfResults(projKey, pageStart)
        (nextPageStart, (names ++ pageOfNames))
      })
  }

  private def repoNamesFor(projectKey: String) = {
    val names = if (projKeys.contains(projectKey)) repoNamesByProjKey.getOrElse(projectKey, getRepoNames(projectKey)) else Set[String]()
    repoNamesByProjKey += (projectKey -> names)
    names
  }

  private def slugify(name: String) = {
    Normalizer.normalize(name, Normalizer.Form.NFKD)
      .replaceAll("[^\\p{InBasicLatin}]+", "")
      .replaceAll("[^a-zA-Z\\-_0-9\\.]+", "-").toLowerCase(Locale.US)
  }

  private def cloneUrlFor(projKey: String, repoName: String): String = {
    val url = new URL(stashURL)
    "%s://%s:%s@%s:%d%s/scm/%s/%s.git".format(url.getProtocol, username, password, url.getHost, url.getPort, url.getPath, projKey, slugify(repoName))
  }

  private def searchForUnused(prefix: String, existing: Set[String]) = {
    //Enough characters for "-num"
    def prefixLength(num: Int): Int = java.lang.Math.floor(java.lang.Math.log10(num)).asInstanceOf[Int]

    Stream.from(1).map(num => (prefix.take(64 - prefixLength(num) + 1) + ("-%d")).format(num)).dropWhile(existing.contains(_)).head
  }

  private def chooseRepoName(projKey: String, name: String, existing: Set[String]): Either[String, String] = {
    val legalName = toLegalRepoName(name).getOrElse(searchForUnused("imported-repo", existing))
    if (existing.contains(legalName)) {
      repoHasCommits(projKey, slugify(legalName)).fold(
        error => Left("Unable to choose a repository name: %s".format(error)),
        hasCommits => if (hasCommits) Right(searchForUnused(legalName, existing)) else Right(legalName))
    } else {
      Right(legalName)
    }
  }

  def ensureProjectCreated(projKey: String, projName: String) : Either[String, Boolean] = {
    if (!hasProjectWithKey(projKey) && !hasProjectWithName(projName)) {
      if (!isValidProjectKey(projKey)) {
        Left("The project key \"%s\" is invalid.".format(projKey))
      } else if (!isValidProjectName(projName)) {
        Left("The project name \"%s\" is invalid.".format(projName))
      } else {
        try {
          createProject(projKey, projName)

          projKeys += projKey
          projNames += projName

          logger.log("Project \"%s\" created".format(projKey))

          Right(true)
        } catch {
          case e: Exception => Left("Failed to create project \"%s\": %s".format(projKey, e.getMessage))
        }
      }
    } else {
      Right(false)
    }
  }

  def createProject(projKey: String, projName: String) {
    resource.path("/projects").`type`(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
      .entity(new JSONObject(Map("key" -> projKey, "name" -> projName)).toString()).post(classOf[String])
  }

  def ensureRepoCreated(projKey: String, repoName: String, failIfAlreadyExists: Boolean = true): Either[String, (Boolean, String)] =
    if (!hasRepoWithName(projKey, repoName)){
      if (!isValidRepoName(repoName)) {
        Left("The project with key \"%s\" does not exist. You must also supply the project name so that it can be created.".format(projKey))
      } else {
        try {
          val response: String =
            resource.path("/projects/%s/repos".format(projKey))
              .`type`(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
              .entity(new JSONObject(Map("name" -> repoName)).toString())
              .post(classOf[String])

          logger.log("Repository \"%s\" in project \"%s\" created".format(repoName, projKey))

          repoNamesByProjKey += (projKey -> (repoNamesByProjKey.getOrElse(projKey, Set[String]()) + repoName))

          Right(true, JSON.parseFull(response).get.asInstanceOf[Map[String, Any]]("cloneUrl").asInstanceOf[String])
        } catch {
          case e: Exception => Left("Failed to create repository \"%s\" in project \"%s\": %s".format(repoName, projKey, e.getMessage))
        }
      }
    } else if (failIfAlreadyExists) {
      Left("A repository with name \"%s\" in project \"%s\" already exists".format(repoName, projKey))
    } else {
      Right((false, cloneUrlFor(projKey, repoName)))
    }

  def getRepoState(projKey: String, repoSlug: String): RepoState = {
    val path = "/projects/%s/repos/%s".format(projKey, repoSlug)
    try {
      val response = resource.path(path).`type`(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get(classOf[String])
      fromStashName(JSON.parseFull(response).get.asInstanceOf[Map[String, Any]]("state").asInstanceOf[String])
    }
    catch {
      case e: Exception => {
        logger.log("Failed to check state of repository at %s: %s".format(path, e.getMessage), Some(e))
        Unknown
      }
    }
  }

  def repoHasCommits(projKey: String, repoSlug: String): Either[String, Boolean] = {
    val path = "/projects/%s/repos/%s/commits".format(projKey, repoSlug)

    def logAndReturnError(e: Exception): String = logger.logReturn("Failed to check if repository has commits at %s: %s".format(path, e.getMessage), Some(e))

    try {
      Right(fetchPage(path, response => response("values").asInstanceOf[Seq[Any]].size > 0)._2)
    }
    catch {
      case e: UniformInterfaceException => if (e.getResponse.getClientResponseStatus.getStatusCode == 404) Right(false) else Left(logAndReturnError(e))
      case e: Exception => Left(logAndReturnError(e))
    }
  }

  def reserveRepoName(projKey: String, repoName: String): Either[String, String] = {
    val repoNames = repoNamesFor(projKey)
    chooseRepoName(projKey, repoName, repoNames).fold(
      error => Left("Unable to reserve a repository name: %s".format(error)),
      name => {
        repoNamesByProjKey += (projKey -> (repoNames + name))
        Right(name)
      })
  }

  def toLegalProjectKey(name: String) = {
    val cleaned = name.replaceAll("^[^\\p{Alpha}]", "").replaceAll("[^\\p{Alnum}_]", "").toUpperCase(Locale.US).take(64)
    if (cleaned.isEmpty) None else Some(cleaned)
  }

  def toLegalProjectName(name: String) = {
    val cleaned = name.take(64)
    if (cleaned.isEmpty) None else Some(cleaned)
  }

  def toLegalRepoName(name: String) = {
    val cleaned = name.replaceAll("^[^\\p{Alnum}]", "").replaceAll("[^\\w\\-\\. ]", "").take(64)
    if (cleaned.isEmpty) None else Some(cleaned)
  }

  def isValidProjectKey(key: String) = key.matches("[a-zA-Z][a-zA-Z0-9_]*"/*Project.KEY_REGEXP*/) && key.length() <= 64

  def isValidProjectName(name: String) = !name.isEmpty && name.length() <= 64

  def isValidRepoName(name: String) = name.matches("[\\p{Alnum}][\\w\\-\\. ]*"/*Repository.NAME_REGEXP*/) && name.length() <= 64

  def hasProjectWithKey(key: String): Boolean = {
    projKeys.contains(key)
  }

  def hasProjectWithName(name: String): Boolean = {
    projNames.contains(name)
  }

  def hasRepoWithName(projectKey: String, name: String): Boolean = {
    repoNamesFor(projectKey)
    repoNamesByProjKey(projectKey).contains(name)
  }

  def getProjectNameForKey(key: String): Option[String] = projKeysAndNames.get(key)

  def chooseProjectName() = searchForUnused("imported-project", projKeys)

  def chooseProjectKey() = searchForUnused("imported-project", projNames)

  def chooseUnusedProjectName(projName: String) = searchForUnused(projName, projNames)

  def chooseRepoName(projKey: String) = searchForUnused("imported-repo", repoNamesFor(projKey))
}
