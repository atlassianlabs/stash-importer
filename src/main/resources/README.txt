==============
Stash Importer
==============

The Stash Importer allows you to import multiple repositories from your local disk into a running Stash instance. Stash
Importer is used iteratively - first to locate the repositories on disk allowing you to tailor the defaults it assigns
each repository, then to import and thereafter to iteratively retry importing any previous failures.

-------
License
-------

See license.txt for license details

--------------------
Minimum requirements
--------------------

* Java 6
* Git 1.7.3
* Stash 2.4

------------
Installation
------------

Unzip the Stash Import zip file to a directory on the same system as the Git repositories you wish to import.

-----
Usage
-----

To invoke the importer and list its supported commands, run
bin/stash-import.sh --help or bin\stash-import.bat --help e.g.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> ./bin/stash-import.sh --help
Usage:
	stash-import --version
	stash-import --help [command]
	stash-import <command> [OPTIONS] <arguments>

Supported commands are:
	createlist	Detects git repositories in the local file system and generates an import list file for use with the importlist command.
	importlist	Imports multiple git repositories from the local filesystem into Stash. Uses an import list file created by the createlist command.
	import	Imports a single git repository from the local filesystem into Stash.

See stash-import --help <command> for more information on a specific command.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-----------------------------------
Cloning repositories to be imported
-----------------------------------

Before starting an import you must first clone all the repositories you wish to import onto your local disk.

------------------------------
Generating an import list file
------------------------------

The first step in importing is to generate an import list file from these local repositories. The import list file
records the repository locations on disk, the project keys, names and repository slugs the Stash
Importer will import them to and the current status of the import process for each repository (starting with 'Pending'
meaning the import has not yet occurred).

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> ./bin/stash-import.sh createlist http://stashhost:7990/path/to/stash import.list path/to/repo1 path/to/repo2 path/to/dir/containing/repos
Enter username for [http://stashhost:7990/path/to/stash]: adminusername
Enter password for [http://stashhost:7990/path/to/stash]:
Looking for git repositories in "path/to/repo1", "path/to/repo2", "path/to/lots/of/repos".
Found git repository "/full/path/to/repo1".
Found git repository "/full/path/to/repo2".
Found git repository "/full/path/to/dir/containing/repos/repoA".
Found git repository "/full/path/to/dir/containing/repos/repoB".

Import list for 4 repositories was saved to "/full/path/to/import.list".
Please manually inspect this file, amending any import entries as necessary, and then run the importlist command on this file to start the import.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you don't supply the username and the password of an administrative Stash user via the --username and --password
options, you will be prompted for them. Once completed, this is what the import list file looks like:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Each line represents a local repository that has already been or will
## be imported along with their project key, project name and repository name
##
## If a repository is not yet imported its state will be Pending
## If a repository has already been successfully imported its state will be Succeeded
## If a repository failed to import last time its state will be Failed
## A comment may also be present explaining the reason for the current state
##
## Failed repository imports will be retried the next time the importlist command is run.
## Repositories not yet imported will be attempted the next time the importlist command is run
## Already imported repositories will be ignored
##
## Columns:
## directory, project key, project name, repository name, import state, comments

/full/path/to/repo1, REPO1, REPO1, repo1, Pending, Not yet imported
/full/path/to/repo2, REPO2, REPO2, repo2, Pending, Not yet imported
/full/path/to/dir/containing/repos/repoA, REPOA, REPOA, repoa, Pending, Not yet imported
/full/path/to/dir/containing/repos/repoB, REPOB, REPOB, repob, Pending, Not yet imported

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By default the Stash Importer will assign a new and unique project name, project key and repository slug to each
repository to be imported. If project names, keys or repository slugs already exist that conflict with the ones
derived from your local repository clones, a new one will be used.

If you wish to import all repositories into the same new or existing project, use the
--projkey / --projname options as documented in the help for the createlist command.

You are free to edit this file and amend any entries as you require to have them target a new
project name, key or repository slug before you start importing.

---------------------------
Importing your repositories
---------------------------

Once you've edited the import list file to your satisfaction, it's time to start the import process.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> ./bin/stash-import.sh importlist http://stashhost:7990/path/to/stash import.list

Enter username for [http://stashhost:7990/path/to/stash]: admin
Enter password for [http://stashhost:7990/path/to/stash]:
Importing repositories from import list "/full/path/to/import.list"

Importing repository in directory "/full/path/to/repo1" into repository repo1 of project REPO1
Created repository "repo1" in project "REPO1".
Pushing "/full/path/to/repo1" to repository "repo1".
Pushing failed.

Importing repository in directory "/full/path/to/repo2" into repository repo1 of project REPO2
Created repository "repo2" in project "REPO2".
Pushing "/full/path/to/repo2" to repository "repo2".
Pushing succeeded.

Importing repository in directory "/full/path/to/dir/containing/repos/repoA" into repository repoA of project REPOA
Created repository "repoA" in project "REPOA".
Pushing "/full/path/to/dir/containing/repos/repoA" to repository "repoA".
Pushing succeeded.

Importing repository in directory "/full/path/to/dir/containing/repos/repoB" into repository repo1 of project REPOB
Created repository "repoB" in project "REPOB".
Pushing "/full/path/to/dir/containing/repos/repoB" to repository "repoB".
Pushing succeeded.

Imported 3 repositories, 1 repository failed to import, 0 repositories were skipped.
Updated import list written to "/full/path/to/import.list"
Logging information was written to "/var/folders/4j/f9tvzp2x1vv_r925yw364vmm0000gn/T/stash-import.log"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

From the above output it's clear that three repositories imported successfully and one failed to import. If you view the
updated import list file you will see any comments associated with the failed import:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
...

/full/path/to/repo1, REPO1, REPO1, repo1, Failed, An error occurred. See logs for details.
/full/path/to/repo2, REPO2, REPO2, repo2, Succeeded, "/full/path/to/repo2" was pushed to repository "repo2" in project "REPO2".
/full/path/to/dir/containing/repos/repoA, REPOA, REPOA, repoa, Succeeded, "/full/path/to/dir/containing/repos/repoA" was pushed to repository "repoA" in project "REPOA".
/full/path/to/dir/containing/repos/repoB, REPOB, REPOB, repob, Succeeded, "/full/path/to/dir/containing/repos/repoB" was pushed to repository "repoB " in project "REPOB".
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once you have consulted the log file and corrected any problems that prevented the import, you can safely rerun the
previous command which will only attempt to import any repositories not yet successfully imported:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> ./bin/stash-import.sh importlist http://stashhost:7990/path/to/stash import.list

Enter username for [http://stashhost:7990/path/to/stash]: admin
Enter password for [http://stashhost:7990/path/to/stash]:
Importing repositories from import list "/full/path/to/import.list"

Importing repository in directory "/full/path/to/repo1" into repository repo1 of project REPO1
Created repository "repo1" in project "REPO1".
Pushing "/full/path/to/repo1" to repository "repo1".
Pushing succeeded.

Imported 1 repository, 0 repositories failed to import, 0 repositories were skipped.
Updated import list written to "/full/path/to/import.list"
Logging information was written to "/var/folders/4j/f9tvzp2x1vv_r925yw364vmm0000gn/T/stash-import.log"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Success! Now all of your repositories have been imported into Stash.

-----------------------------
Importing a single repository
-----------------------------

Sometimes you may only want to import a single repository into Stash. The Stash Importer's "import" command will perform
this for you without the need for generating import list files:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> ./bin/stash-import.sh importlist http://stashhost:7990/path/to/stash import path/to/repo1

Enter username for [http://stashhost:7990/path/to/stash]: admin
Enter password for [http://stashhost:7990/path/to/stash]:

Importing repository in directory "/full/path/to/repo1" into repository repo1 of project REPO1
Pushing "/full/path/to/repo1" to repository "repo1".
Pushing succeeded.
Importing "/full/path/to/repo1" succeeded: "/full/path/to/repo1" was pushed to repository "repo1" in project "REPO1".
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Along with specifying a different project key and name through the --projname and --projkey options, you can
also specify the repositoy slug throught the --reposlug option if the default slug does not suit or already exists.